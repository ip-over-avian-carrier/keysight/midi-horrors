{
  description = "Midi Horrors beyond Comprehension";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, crane, fenix, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; overlays = [ fenix.overlays.default ]; };

      toolchain = fenix.packages.${system}.complete.withComponents [
        "cargo"
        "rustc"
        "rustfmt"
        "rust-analyzer"
        "rust-src"
      ];

      craneLib = crane.lib.${system}.overrideToolchain toolchain;

      buildInputs = [
          pkgs.xorg.libxcb pkgs.libxkbcommon pkgs.openssl pkgs.glib pkgs.gtk3
          pkgs.libGL
        ];

      midi-horrors = craneLib.buildPackage {
        src = craneLib.cleanCargoSource (craneLib.path ./.);
        strictDeps = true;
        doCheck = false;

        inherit buildInputs;
        nativeBuildInputs = [ pkgs.pkg-config pkgs.wrapGAppsHook ];
      };
    in {
      checks.${system} = { inherit midi-horrors; };

      packages.${system} = {
        midi-horrors = midi-horrors;
        default = midi-horrors;
      };
      apps.${system} = {
        midi-horrors = {
          type = "app";
          program = "${midi-horrors}/bin/midi-horrors";
        };
        default = {
          type = "app";
          program = "${midi-horrors}/bin/midi-horrors";
        };
      };

      devShells.${system}.default =
        craneLib.devShell {
          checks = self.checks.${system};
          inputsFrom = [ midi-horrors ];

          packages = [
            # pkgs.rust-analyzer-nightly
          ];

          LD_LIBRARY_PATH="${pkgs.lib.makeLibraryPath buildInputs}:$LD_LIBRARY_PATH";
        };
    };
}
