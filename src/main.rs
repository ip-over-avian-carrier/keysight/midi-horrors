use anyhow::Context as _;
use eframe::{
    egui::{self, warn_if_debug_build, RichText},
    epaint::Color32,
};
use midly::{
    num::{u15, u28, u4, u7},
    Smf,
};
use rayon::{iter::ParallelIterator as _, slice::ParallelSlice};

fn main() -> anyhow::Result<()> {
    let opts = eframe::NativeOptions::default();
    eframe::run_native("Midi Horrors", opts, Box::new(|cc| Box::new(App::new(cc))))
        .map_err(|e| anyhow::anyhow!(format!("{:?}", e)))
        .context("failed to run app")?;

    Ok(())
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
struct RGB {
    r: u8,
    g: u8,
    b: u8,
}

impl From<image::Rgb<u8>> for RGB {
    fn from(value: image::Rgb<u8>) -> Self {
        RGB {
            r: value.0[0],
            g: value.0[1],
            b: value.0[2],
        }
    }
}

#[derive(Debug, Clone, Copy, Default)]
struct CNote {
    pub vel: u7,
    pub channel: u4,
    pub force_release: bool,
}

struct App {
    input_file: String,
    output_file: String,

    generating: Option<std::sync::mpsc::Receiver<anyhow::Result<()>>>,
    done: bool,

    error: Option<anyhow::Error>,
}

impl App {
    fn new(_cc: &eframe::CreationContext<'_>) -> Self {
        App {
            input_file: String::new(),
            output_file: String::new(),
            generating: None,
            done: false,
            error: None,
        }
    }
}

impl eframe::App for App {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {

        if let Some(ref ch) = self.generating {
            match ch.try_recv() {
                Ok(Ok(())) => { self.done = true; self.generating = None; },
                Ok(Err(why)) => {
                    self.done = true;
                    self.generating = None;
                    self.error = Some(why);
                }
                Err(why) => {
                    match why {
                        std::sync::mpsc::TryRecvError::Empty => (),
                        std::sync::mpsc::TryRecvError::Disconnected => (),
                    }
                }

            }
        }

        let cx = egui::CentralPanel::default().show(ctx, |ui| -> anyhow::Result<()> {
            ui.heading("Midi Horrors");
            warn_if_debug_build(ui);

            ui.add_enabled_ui(self.generating.is_none(), |en| {
                en.label("Source Image:");
                en.horizontal(|h| {
                    h.text_edit_singleline(&mut self.input_file);
                    if h.button("Pick File").clicked() {
                        let nf = rfd::FileDialog::new()
                            .add_filter("Image", &["png", "webp", "jpg", "jpeg"])
                            .add_filter("All Files", &["*"])
                            .pick_file();

                        if let Some(n) = nf {
                            self.input_file = n.to_string_lossy().to_string();
                        }
                    }
                });

                en.label("Destination File:");
                en.horizontal(|h| {
                    h.text_edit_singleline(&mut self.output_file);
                    if h.button("Pick File").clicked() {
                        let nf = rfd::FileDialog::new()
                            .add_filter("Midi File", &["mid"])
                            .add_filter("All Files", &["*"])
                            .save_file();

                        if let Some(n) = nf {
                            self.output_file = n.display().to_string();
                        }
                    }
                });
            });

            if ui
                .add_enabled(
                    self.generating.is_none(),
                    egui::Button::new(if self.generating.is_some() {
                        "Generating..."
                    } else {
                        "Generate!"
                    }),
                )
                .clicked()
            {
                let (tx, rx) = std::sync::mpsc::channel::<anyhow::Result<()>>();
                self.generating = Some(rx);
                self.done = false;

                let i_file = self.input_file.clone();
                let o_file = self.output_file.clone();
                std::thread::Builder::new()
                    .name("gen-thr".to_owned())
                    .spawn(move || {
                        let f = move || -> anyhow::Result<()> {
                            let i_file = i_file;
                            let o_file = o_file;

                            let rgba_img: Vec<_> = {
                                let img = image::io::Reader::open(i_file)
                                    .context("cannot open image")?
                                    .with_guessed_format()
                                    .context("unable to guesstimate format")?
                                    .decode()
                                    .context("cannot decode image")?;
                                img.to_rgb8().pixels().cloned().collect()
                            };

                            let chnks: Vec<_> = rgba_img
                                .par_chunks_exact(52)
                                .map(|chunk| {
                                    let mut r = [CNote::default(); 52];
                                    debug_assert_eq!(chunk.len(), r.len());

                                    for i in 0..r.len() {
                                        let px = RGB::from(chunk[i]);

                                        // Clamp R to 127 max
                                        r[i].vel = u7::new(px.r & 0x7F);
                                        // Channel (G, lower 4 bits)
                                        r[i].channel = u4::new(px.g & 0x0F);
                                        // Force Release (G, topmost bit)
                                        r[i].force_release = (px.g & 0x80) == 0x80;
                                    }

                                    r
                                })
                                .collect();

                            const EVENT_DELTA: u32 = 11;
                            let mut result = Vec::new();
                            for y in 0..52 {
                                let px = chnks[0][y];

                                let note = make_note(note_idx(y as u8), px.vel, px.vel > 0);
                                result.push(midly::TrackEvent {
                                    delta: 0.into(),
                                    kind: midly::TrackEventKind::Midi {
                                        channel: px.channel,
                                        message: note,
                                    },
                                });
                            }

                            let mut delta = 0;
                            for i in 1..chnks.len() {
                                delta += EVENT_DELTA;
                                let mut get_delta = || {
                                    let nd = u28::new(delta);
                                    delta = 0;
                                    nd
                                };

                                for y in 0..52 {
                                    let px = chnks[i - 1][y];
                                    let cx = chnks[i][y];

                                    if px.vel > 0 && (cx.force_release || cx.vel == 0) {
                                        let note = make_note(note_idx(y as u8), 0.into(), false);
                                        result.push(midly::TrackEvent {
                                            delta: get_delta(),
                                            kind: midly::TrackEventKind::Midi {
                                                channel: px.channel,
                                                message: note,
                                            },
                                        });
                                    }

                                    if cx.vel > 0 && px.vel == 0 {
                                        let note = make_note(note_idx(y as u8), cx.vel, true);
                                        result.push(midly::TrackEvent {
                                            delta: get_delta(),
                                            kind: midly::TrackEventKind::Midi {
                                                channel: cx.channel,
                                                message: note,
                                            },
                                        });
                                    }
                                }
                            }

                            let mut deltad = false;
                            let mut get_delta = || {
                                if deltad {
                                    0
                                } else {
                                    deltad = true;
                                    EVENT_DELTA
                                }
                            };
                            for i in 0..52 {
                                let px = chnks[chnks.len() - 1][i];
                                if px.vel > 0 {
                                    let note = make_note(note_idx(i as u8), 0.into(), false);
                                    result.push(midly::TrackEvent {
                                        delta: get_delta().into(),
                                        kind: midly::TrackEventKind::Midi {
                                            channel: px.channel,
                                            message: note,
                                        },
                                    });
                                }
                            }

                            let mut smf = Smf::new(midly::Header::new(
                                midly::Format::SingleTrack,
                                midly::Timing::Metrical(u15::from(96)),
                            ));

                            let mut track = Vec::new();
                            track.push(midly::TrackEvent {
                                delta: 0.into(),
                                kind: midly::TrackEventKind::Meta(
                                    midly::MetaMessage::TimeSignature(4, 4, 24, 8),
                                ),
                            });
                            track.push(midly::TrackEvent {
                                delta: 0.into(),
                                kind: midly::TrackEventKind::Meta(midly::MetaMessage::Tempo(
                                    500000.into(),
                                )),
                            });

                            track.extend(result.into_iter());

                            track.push(midly::TrackEvent {
                                delta: 0.into(),
                                kind: midly::TrackEventKind::Meta(midly::MetaMessage::EndOfTrack),
                            });

                            smf.tracks.push(track);

                            smf.save(o_file).context("unable to write track data")?;

                            Ok(())
                        };

                        let _ = match f() {
                            Ok(_) => tx.send(Ok(())),
                            Err(why) => tx.send(Err(why)),
                        };
                    })
                    .context("unable to spawn generation thread")?;
            }

            {
                // Show the error, but also properly handle the exit cond
                let mut clear_error = false;
                if let Some(e) = &self.error {
                    clear_error = ui.button("Clear Error").clicked();
                    egui::ScrollArea::vertical().show(ui, |ui| {
                        ui.colored_label(
                            Color32::RED,
                            format!("----- ENCOUNTERED ERROR -----\n{:#?}", e),
                        );
                    });
                }

                if clear_error {
                    self.error = None;
                }
            }

            if self.generating.is_some() {
                ui.spinner();
            }

            Ok(())
        });

        if let Err(why) = cx.inner {
            self.error = Some(why);
        };
    }
}

fn note_idx(key: u8) -> u7 {
    let v = match key {
        0 => 21,
        1 => 23,
        2 => 24,
        3 => 26,
        4 => 28,
        5 => 29,
        6 => 31,
        7 => 33,
        8 => 35,
        9 => 36,
        10 => 38,
        11 => 40,
        12 => 41,
        13 => 43,
        14 => 45,
        15 => 47,
        16 => 48,
        17 => 50,
        18 => 52,
        19 => 53,
        20 => 55,
        21 => 57,
        22 => 59,
        23 => 60,
        24 => 62,
        25 => 64,
        26 => 65,
        27 => 67,
        28 => 69,
        29 => 71,
        30 => 72,
        31 => 74,
        32 => 76,
        33 => 77,
        34 => 79,
        35 => 81,
        36 => 83,
        37 => 84,
        38 => 86,
        39 => 88,
        40 => 89,
        41 => 91,
        42 => 93,
        43 => 95,
        44 => 96,
        45 => 98,
        46 => 100,
        47 => 101,
        48 => 103,
        49 => 105,
        50 => 107,
        51 => 108,
        _ => 21,
    };
    u7::new(v)
}

fn make_note(key: u7, vel: u7, on: bool) -> midly::MidiMessage {
    if on {
        midly::MidiMessage::NoteOn { key, vel }
    } else {
        midly::MidiMessage::NoteOff { key, vel }
    }
}
